// Update with your config settings.

module.exports = {

  client: 'postgresql',
  connection: {
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT 
  },
  pool: {
    min: 0,
    max: 10
  },
  migrations: {
    tableName: 'knex_migrations'
  }

};
