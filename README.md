# Api Users

Repository of node-js server api for management of users

## Setup

#### Create the `.env` file with the information below. 
```bash
    NODE_ENV=development
    PORT=${PORT}
    SECRET_API_KEY=${API_KEY}

    POSTGRES_USER=${USER_DB}
    POSTGRES_PASSWORD=${PASSWORD_DB}
    POSTGRES_DB=${NAME_DB}
    POSTGRES_HOST=${HOST_DB}
```

#### Create the `database.env` file with the information below. 

```bash
    POSTGRES_USER=${USER_DB}
    POSTGRES_PASSWORD=${PASSWORD_DB}
    POSTGRES_DB=${NAME_DB}
    POSTGRES_HOST=${HOST_DB}
```

### Installation/Build

1) Install required packages:

- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/compose/install/)

2) Clone the repository into your desired directory.

### Running

Building and starting the containers in docker-compose:
```sh
$ cd [your workspace directory]/api-user
$ docker-compose up -d --build
```

Check if two docker containers (api-users_app_1 ,  api-users_db_1) are running:
```sh
$ docker-compose ps 
```

To stop containers and remove images, execute:
```sh
$ docker-compose down -v --rmi 'local'
```



