module.exports = app => {

  app.route('/')
    .get((_, res) => {
      res.status(200).json({message: 'REST Fullstack Challenge 20201209 Running'})
    })

  /*
  ===========================================================
                       Routers users
  ===========================================================
  */
  app.route('/users')
    .get(app.api.controllers.userController.index)
    .post(app.api.controllers.userController.store)

  app.route('/users/:id')
    .get(app.api.controllers.userController.show)
    .put(app.api.controllers.userController.update)
    .delete(app.api.controllers.userController.destroy)


}