const bodyParse = require('body-parser')
const cors = require('cors')
const basicAuth = require ('express-basic-auth') 



module.exports = app => {
  app.use(bodyParse.json())
  app.use(cors())
  app.use (basicAuth ({ 
    users: {'admin': process.env.SECRET_API_KEY},
    unauthorizedResponse: 'Not authorized'
  }))
}