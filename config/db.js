const config = require('../knexfile.js')
const knex = require('knex')(config)

const { attachPaginate } = require('knex-paginate');
attachPaginate();
//knex.migrate.latest([config])
module.exports = knex