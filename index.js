const app = require('express')()
const consign = require('consign')
const db = require('./config/db')
const port = process.env.PORT || '3333';

app.db = db


consign()
  .include('./config/middlewares.js')
  .then('./api/helpers')
  .then('./api/models')
  .then('./api/controllers')
  .then('./config/routes.js')
  .into(app)

app.listen(port, ()=> {
  console.log(`Running api Tatic in port ${port}`)
})

