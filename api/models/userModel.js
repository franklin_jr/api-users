module.exports = app => {

  const getAll = (params) => {
    const { page, PerPage, search, gender } = params
    const peg = app.api.helpers.pagination.makePagination(page, PerPage)
    return new Promise((resolve, reject) => {
      var query = app.db('users')

      if(search)
        query.whereRaw(`LOWER(name->>'first') LIKE LOWER('%${search}%') OR LOWER(name->>'last') LIKE LOWER('%${search}%')  OR LOWER(nat) LIKE LOWER('%${search}%')`)
      if(gender)
        query.where('gender', gender)

      query
      .paginate({ perPage: peg.perPage, currentPage: peg.page, isLengthAware: true })
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        reject(err)
      })
    })  
  }

  const get = (id) => {
    return new Promise((resolve, reject) => {
      app.db('users')
      .where('id', id)
      .first()
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        reject(err)
      })
    })  
  }

  const insert = (data) => {
    return new Promise((resolve, reject) => {
      app.db('users')
      .insert(data)
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        reject(err)
      })
    })  
  }

  const update = (id, data) => {
    return new Promise((resolve, reject) => {
      app.db('users')
      .update(data)
      .where('id', id)
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        reject(err)
      })
    })  
  }

  const delet = (id) => {
    return new Promise((resolve, reject) => {
      app.db('users')
      .delete()
      .where('id', id)
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        reject(err)
      })
    })  
  }



  return { getAll, get, insert, update, delet }
}