
module.exports = app => {
  const makePagination  = (page, PerPage) => {
    if(!page)
        page = 1 
    if(!PerPage)
        PerPage = 50
  
    return {
        page: page, 
        perPage: PerPage
    }
  }

  return {
    makePagination
  }
}