
module.exports = app => {

  const index = (req, res) => {
    app.api.models.userModel.getAll(req.query)
      .then(resp => {
        res.status(200).json(resp)
      })
      .catch(err => {
        res.status(404).json({ err, message: 'Error loading users' })
      })
  }

  const show = (req, res) => {
    let { id } = req.params
    app.api.models.userModel.get(id)
    .then(resp => {
      res.status(200).json(resp)
    })
    .catch(err => {
      res.status(404).json({ err, message: 'Error get user' })
    })
  }


  const store = (req, res) => {
    app.api.models.userModel.insert(req.body)
      .then(resp => {
        res.status(200).json(resp)
      })
      .catch(err => {
        res.status(404).json({ err, message: 'Error inserting user' })
      })
  }

  const update = (req, res) => {
    let { id } = req.params
    app.api.models.userModel.update(id, req.body)
    .then(resp => {
      res.status(200).json(resp)
    })
    .catch(err => {
      res.status(404).json({ err, message: 'Error updating user' })
    })
  }

  const destroy = (req, res) => {
    let { id } = req.params
    app.api.models.userModel.delet(id)
    .then(resp => {
      res.status(200).json(resp)
    })
    .catch(err => {
      res.status(404).json({ err, message: 'Error delete user' })
    })
  }


  return { index, show, store, update, destroy }
}