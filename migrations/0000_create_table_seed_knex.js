exports.up = async (knex) => {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
  return knex.schema
    .createTable('seed_knex',  (table) => {
      table.uuid('id').unique().notNullable().primary().defaultTo(knex.raw('uuid_generate_v4()'));
      table.string('name').notNullable().unique()
      table.timestamps()
      table.timestamp('deleted_at')
    })
};

exports.down = (knex) => {
  return knex.schema.dropTable('seed_knex')
};
