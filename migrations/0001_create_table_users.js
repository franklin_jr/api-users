exports.up = async (knex) => {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
  return knex.schema
    .createTable('users',  (table) => {
      table.uuid('id').unique().notNullable().primary().defaultTo(knex.raw('uuid_generate_v4()'));
      table.string('gender')
      table.string('phone')
      table.string('email').unique()
      table.string('cell')
      table.string('nat')
      table.json('name').notNullable()
      table.json('location')
      table.json('login')
      table.json('dob')
      table.json('registered')
      table.json('picture')
      table.timestamps()
      table.timestamp('deleted_at')
    })
};

exports.down = (knex) => {
  return knex.schema.dropTable('users')
};
