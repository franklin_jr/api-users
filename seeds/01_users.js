const users = require('./users.json')
const nameKn = '01_users'
exports.seed = function(knex) {

  return knex('seed_knex')
    .where('name', nameKn)
    .first()
    .then((resp) => {
      if(!resp) {
        var usersMap = users.results

        usersMap.map((item) => {
          delete item.id
          item.name = JSON.stringify(item.name)
          item.location = JSON.stringify(item.location)
          item.login = JSON.stringify(item.login)
          item.dob = JSON.stringify(item.dob)
          item.registered = JSON.stringify(item.registered)
          item.picture = JSON.stringify(item.picture)
        })
        
        return knex('users').insert(usersMap).then(() => {
          return knex('seed_knex').insert({name: nameKn})
        });
      }
    })

 
};
